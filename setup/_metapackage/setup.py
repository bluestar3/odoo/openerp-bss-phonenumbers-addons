import setuptools

with open('VERSION.txt', 'r') as f:
    version = f.read().strip()

setuptools.setup(
    name="odoo10-addons-odoo10-addons-bss-phonenumbers",
    description="Meta package for odoo10-addons-bss-phonenumbers Odoo addons",
    version=version,
    install_requires=[
        'odoo10-addon-bss_crm_phonenumbers',
        'odoo10-addon-bss_partner_multi_phone',
        'odoo10-addon-bss_partner_phonenumbers',
        'odoo10-addon-bss_phonenumbers',
    ],
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Odoo',
    ]
)
